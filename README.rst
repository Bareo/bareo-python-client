bareo
-----

.. image:: https://travis-ci.org/Bareo/bareo-python-client.svg?branch=master
   :target: https://travis-ci.org/Bareo/bareo-python-client


Bareo.io python API client library

Copyright 2014, 2015, 2016 Bareo LLC
Licensed under the GNU GPVLv3 or later

See `AUTHORS` for a full list of contributors
See `COPYING` for the full text of the license

To use (with caution), simply do::

>>> import bareo
>>> apiClient = bareo.BareoAPI()
>>> apiClient.user.username = "test"
>>> apiClient.user.password = "test
>>> response = apiClient.authenticate()
>>> print(apiClient.user)
>>> apiClient.fetch_items()
>>> print(api.items)
>>> apiClient.fetch_item(3)
>>> print(next(item for item in api.items if int(item['id']) == 3))

See https://docs.bareo.io for full usage documentation.
