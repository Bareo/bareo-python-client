'''
bareo - Bareo.io python API client library

Copyright 2014, 2015, 2016 Bareo LLC
See AUTHORS file for full list of contributors

This file is part of bareo.

bareo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

bareo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bareo.  If not, see <http://www.gnu.org/licenses/>.

'''

import urllib3
import urllib.request
import json
from collections import UserDict

BASEURL = "http://api.bareo.hiphop/v2/"
#BASEURL = "https://api.bareo.io/v2/"
#BASEURL = "http://localhost:8080/v2/"

auth_token = None

def _make_request(_url="", _method="POST", _data=None):
    global auth_token
    print ("Making call to: " + _url)
    # Set the request headers as appropriate
    headers = {}

    # Add the X-Authorization header if needed
    if (("auth" in _url) and (_method == "POST")) or (("users" in _url) and (_method == "POST")):
        headers['Content-Type'] = 'application/json'
    else:
        headers['Content-Type'] = 'application/json'
        headers['X-Authorization'] = auth_token

    if (_method == "POST" or _method == "PUT"):
        # Convert _data dictionary to JSON
        json_data =json.dumps(_data)
        # Convert str to bytes and ensure the encoding is OK
        post_data = json_data.encode('utf-8')
        # Set the Content-Length header appropriately
        # Da fuq? Setting the Content-Length header is causing json
        # decode errors in the API
        #headers['Content-Length'] = len(_data)
        # Make the request
        request = urllib.request.Request(_url,
                                         post_data,
                                         headers,
                                         method=_method)
    else:
        # Make the request with no post body
        request = urllib.request.Request(_url,
                                         headers=headers,
                                         method=_method)

    # Send the request
    try:
        response = urllib.request.urlopen(request)
    except urllib.error.HTTPError as resp:
        response = resp
        print("ERROR: %s"%resp)

    return response

def _create_url(slug=None, uri_parts=[]):
    url = BASEURL
    if slug == "auth":
        url += "auth/"
    elif slug == "items":
        url += "items/"
    elif slug == "users":
        url += "users/"
    elif slug == "recipes":
        url += "recipes/"
    elif slug == "accounts":
        url += "accounts/"
    elif slug == "sales":
        url += "sales/"

    for uri_part in uri_parts:
        url += str(uri_part) + "/"

    return url

class BareoAPI:
    global BASEURL

    def __init__(self):
        self.user = None
        self.items = []
        self.recipes = []
        self.accounts = []
        self.stock = []
        self.sales = []
        self.auctions = []
        self.images = []

    def create_user(self):
        url = self._create_url(slug="users")
        response = self._make_request(url, "POST", _data={
            "username": self.user.username,
            "password": self.user.password,
            "email": self.user.email,
            "phone": self.user.phone,
            "privilege": self.user.privilege
        })

        ret_val = json.loads(response.read().decode('utf-8'))
        return ret_val

    def fetch_items(self):
        url = _create_url(slug="items")
        response = _make_request(url, "GET")

        ret_val = json.loads(response.read().decode('utf-8'))

        for item in ret_val['items']:
            new_item = BareoItem()
            new_item.update(item)
            self.items.append(new_item)


    def fetch_item(self, item_id):
        item = next((item for item in self.items if int(item['id']) == item_id))
        return item.fetch()

    def fetch_user(self, user_id):
        uri_comps = [user_id]
        url = self._create_url(slug="users", uri_parts=uri_comps)
        response = self._make_request(url, "GET")

        ret_val = json.loads(response.read().decode('utf-8'))
        return ret_val

    def fetch_recipes(self):
        url = _create_url(slug="recipes")
        response = _make_request(url, "GET")

        ret_val = json.loads(response.read().decode('utf-8'))

        for recipe in ret_val['recipes']:
            new_recipe = BareoRecipe()
            new_recipe.update(recipe)
            self.recipes.append(new_recipe)

    def fetch_recipe(self, recipe_id):
        recipe = next((recipe for recipe in self.recipes if int(recipe['id']) == recipe_id))
        return recipe.fetch()

    def fetch_accounts(self):
        url = _create_url(slug="accounts")
        response = _make_request(url, "GET")

        ret_val = json.loads(response.read().decode('utf-8'))

        for account in ret_val['accounts']:
            new_account = BareoAccount()
            new_account.update(account)
            self.accounts.append(new_account)

    def fetch_account(self, account_id):
        account = next((account for account in self.accounts if int(account['id']) == account_id))
        return account.fetch()

    def fetch_sales(self):
        url = _create_url(slug="sales")
        response = _make_request(url, 'GET')

        ret_val = json.loads(response.read().decode('utf-8'))

        for sale in ret_val['sales']:
            new_sale = BareoSale()
            new_sale.update(sale)
            self.sales.append(new_sale)

    def fetch_sale(self, sale_id):
        sale = next((sale for sale in self.sales if int(sale['id']) == sale_id))
        return sale.fetch()

class BareoUser(UserDict):
    global auth_token

    def __init__(self, data=None):
        UserDict.__init__(self)
        self.data = {
            'id': 0,
            'username': '',
            'password': '',
            'email': '',
            'phone': '',
            'privilege': 0,
            'auth_token': None,
            'accounts': [],
        }
        if data is not None: self.update(data)

    def authenticate(self):
        global auth_token
        url = _create_url(slug="auth")
        response = _make_request(url, "POST",
                                 _data={"username":self['username'],
                                        "password":self['password']})

        ret_data = json.loads(response.read().decode("utf-8"))

        if 'auth_token' in ret_data and ret_data['auth_token'] is not "":
            self.update(ret_data)
            auth_token = self['auth_token']
        else:
            auth_token = None

    def deauthenticate(self):
        global auth_token
        uri_comps = [self['id']]
        url = _create_url(slug="auth",uri_parts=uri_comps)

        response = _make_request(url, "DELETE")

        ret_data = response.read().decode("utf-8")
        self['auth_token'] = None
        auth_token = None

    def toJSON(self):
        return json.dumps(self.data, sort_keys=True)


class BareoItem(UserDict):
    def __init__(self, data=None):
        UserDict.__init__(self)
        self.data = {
            'id': 0,
            'name': '',
            'description': '',
            'price': 0.0,
            'account_id': 0,
            'image_id': 0,
            'units': '',
            'upc': '',
            'threshold': 0.0,
            'expirations': [],
        }
        if data is not None: self.update(data)

    def fetch(self):
        uri_comps = [str(self['id'])]
        url = _create_url(slug="items",uri_parts=uri_comps)
        response = _make_request(url, "GET")

        ret_val = json.loads(response.read().decode('utf-8'))
        self.update(ret_val)
        return self

    def toJSON(self):
        return json.dumps(self.data, sort_keys=True)


class BareoItemExpiration(UserDict):
    def __init__(self, data=None):
        UserDict.__init__(self)
        self.data = {
            'id': 0,
            'item_id': 0,
            'account_id': 0,
            'shelf_life': '',
            'quantity': 0.0,
            'units': '',
            'location': '',
        }
        if data is not None: self.update(data)

    def toJSON(self):
        return json.dumps(self.data, sort_keys=True)


class BareoRecipe(UserDict):
    def __init__(self, data=None):
        UserDict.__init__(self)
        self.data = {
            'id': 0,
            'name': '',
            'account_id': 0,
            'recipe_yield': 0.0,
            'shelf_life': '',
            'production_time': 0.0,
            'material_cost': 0.0,
            'items': [],
            'prices': [],
        }
        if data is not None: self.update(data)

    def fetch(self):
        uri_comps = [str(self['id'])]
        url = _create_url(slug="recipes", uri_parts=uri_comps)
        response = _make_request(url, "GET")

        ret_val = json.loads(response.read().decode('utf-8'))
        self.update(ret_val)
        return self

    def toJSON(self):
        return json.dumps(self.data, sort_keys=True)


class BareoRecipePrice(UserDict):
    def __init__(self, data=None):
        UserDict.__init__(self)
        self.data = {
            'id': 0,
            'recipe_id': 0,
            'quantity': 0,
            'price': 0.0,
        }
        if data is not None: self.update(data)

    def toJSON(self):
        return json.dumps(self.data, sort_keys=True)


class BareoRecipeItem(UserDict):
    def __init__(self, data=None):
        UserDict.__init__(self)
        self.data = {
            'id': 0,
            'recipe_id': 0,
            'item_id': 0,
            'quantity': 0.0,
            'units': '',
        }
        if data is not None: self.update(data)

    def toJSON(self):
        return json.dumps(self.data, sort_keys=True)


class BareoAccount(UserDict):
    def __init__(self, data=None):
        UserDict.__init__(self)
        self.data = {
            'id': 0,
            'account_name': '',
            'payment_token': '',
            'account_type': 0,
            'allowed': 0,
            'users': [],
        }
        if data is not None: self.update(data)

    def fetch(self):
        uri_comps = [str(self['id'])]
        url = _create_url(slug="accounts", uri_parts=uri_comps)
        response = _make_request(url, "GET")

        ret_val = json.loads(response.read().decode('utf-8'))
        self.update(ret_val)
        return self

    def toJSON(self):
        return json.dumps(self.data, sort_keys=True)


class BareoStock(UserDict):
    def __init__(self, data=None):
        UserDict.__init__(self)
        self.data = {
            'id': 0,
            'recipe_id': 0,
            'quantity': 0.0,
            'shelf_life': '',
            'account_id': 0,
            'url': '',
        }
        if data is not None: self.update(data)

    def toJSON(self):
        return json.dumps(self.data, sort_keys=True)


class BareoImage(UserDict):
    def __init__(self, data=None):
        UserDict.__init__(self)
        self.data = {
            'id': 0,
            'name': '',
            'path': '',
            'account_id': 0,
            'ext': '',
        }
        if data is not None: self.update(data)

    def toJSON(self):
        return json.dumps(self.data, sort_keys=True)


class BareoSale(UserDict):
    def __init__(self, data=None):
        UserDict.__init__(self)
        self.data = {
            'id': 0,
            'name': '',
            'status': 0,
            'order_date': '',
            'finish_date': '',
            'price': 0.0,
            'material_cost': 0.0,
            'account_id': 0,
            'paid': False,
            'objects': {
                'items': [],
                'recipes': [],
            },
        }
        if data is not None: self.update(data)

    def fetch(self):
        uri_comps = [str(self['id'])]
        url = _create_url(slug="sales", uri_parts=uri_comps)
        response = _make_request(url, 'GET')

        ret_val = json.loads(response.read().decode('utf-8'))
        self.update(ret_val)
        return self

    def toJSON(self):
        return json.dumps(self.data, sort_keys=True)


class BareoSaleItem(UserDict):
    def __init__(self, data=None):
        UserDict.__init__(self)
        self.data = {
            'id': 0,
            'item_id': 0,
            'sale_id': 0,
            'quantity': 0.0,
        }
        if data is not None: self.update(data)

    def toJSON(self):
        return json.dumps(self.data, sort_keys=True)


class BareoSaleRecipe(UserDict):
    def __init__(self, data=None):
        UserDict.__init__(self)
        self.data = {
            'id': 0,
            'recipe_id': 0,
            'sale_id': 0,
            'quantity': 0.0,
        }
        if data is not None: self.update(data)

    def toJSON(self):
        return json.dumps(self.data, sort_keys=True)

