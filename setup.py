'''
bareo - Bareo.io python API client library

Copyright 2014, 2015, 2016 Bareo LLC
See AUTHORS file for full list of contributors

This file is part of bareo.

bareo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

bareo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bareo.  If not, see <http://www.gnu.org/licenses/>.

'''

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

def readme():
    with open('README.rst') as f:
        return f.read()

setup(name='bareo',
      version='0.1',
      description='Bareo.io python API client library',
      url='https://github.com/bareo/bareo-python-client',
      author='zach wick',
      author_email='zwick@bareo.io',
      license='GPLv3+',
      packages=['bareo'],
      zip_safe=False,
      classifiers=[
          'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
          'Intended Audience :: Developers',
          'Programming Language :: Python :: 3.4',
      ],
      install_requires=[
          'urllib3',
      ],
      keywords='bareo inventory forecasting',
      include_package_data=True,
      test_suite='nose.collector',
      tests_require=['nose', 'nose-cover3'],
  )
