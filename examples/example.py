#!/usr/bin/env python3

from bareo import *
from sys import exit
def main():
    api = BareoAPI()
    user = BareoUser({'username':'test',
                      'password':'zwick'})
    api.user = user

    # Authenticate the user
    api.user.authenticate()
    print(api.user)

    # # Fetch all items that the logged in user has access to
    # print()
    # api.fetch_items()
    # print(api.items)

    # # Fetch (and internally update) the item with id=3
    # # NB: The API ensures that the user making this request can access the
    # #     id=3
    # print()
    # item3 = api.fetch_item(3)
    # print(item3)

    # # Fetch all recipes that the logged in user has access to 
    # print()
    # api.fetch_recipes()
    # print(api.recipes)

    # # Fetch (and internally update) the recipe with id=9
    # # NB: the API ensures that the user making this request can access the
    # #     recipe with id=9
    # print()
    # recipe9 = api.fetch_recipe(9)
    # print(recipe9)

    # print()
    # api.fetch_accounts()
    # print(api.accounts)

    # print()
    # account1 = api.fetch_account(1)
    # print(account1)

    print()
    api.fetch_sales()
    print(api.sales)

    print()

    #print()
    #api.create_user()

    print()
    response = api.deauthenticate()

if __name__ == "__main__":
    main()
